# [Jasa Pembuatan Website](https://www.okabrionz.com) Profesional di Indonesia

[![Oka bRionZ](https://www.okabrionz.com/img/opengraph.jpg)](https://www.okabrionz.com/)

### Tentang Oka bRionZ
Oka bRionZ menyediakan jasa pembuatan website untuk Landing Page, Profil Perusahaan, Situs Blogging, Situs Portal Berita, dan e-Commerce.
Saya mengutamakan pembuatan website **tanpa CMS** _(Content Management System)_ seperti;
- [WordPress](https://wordpress.org)
- [Blogger](https://www.blogger.com)
- [Prestashop](https://www.prestashop.com)
- [Joomla](https://joomla.org), dll.

Pembuatan situs secara manual itu lebih mudah untuk _meng-optimasi_ kecepatan situs itu sendiri. Karena, semua kode dapat dikendalikan.

Saya suka membuat situs dengan AMP, Sedikit banyak juga saya menguasai Bootstrap 3.7, Bootstrap 4 Alpha & Beta, MDBootstrap, Material Kits, Material Design Lite dari Google (getmdl.io), Angular

Adapun biaya yang diterapkan untuk jasa pembuatan website oleh Saya, bisa di lihat [disini](https://www.okabrionz.com/biaya-pembuatan-website)

### Kontak

Anda bisa menghubungi saya via kontak berikut:

- [Tel](tel:+6282319228666)
- [Whatsapp](https://wa.me/6282319228666)
- [Facebook Messenger](https://m.me/AntiHacx)
- [Telegram](https://t.me/bRionZ)