User-Agent: *
Disallow: /admin/ 
Allow: /

User-Agent: Googlebot
Allow: /

User-Agent: Googlebot-Mobile
Allow: /

User-Agent: Googlebot-Image
Allow: /img/

User-Agent: msnbot
Disallow: /img/

User-Agent: Teoma
Disallow: /

User-Agent: Adsbot-Google
Allow: /