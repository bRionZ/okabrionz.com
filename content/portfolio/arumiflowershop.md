---
title: "Arumi Flower Shop"
description: "Salah satu Toko Bunga= yang berada di *Jakarta* ingin meningkatkan pendapatannya dengan cara mempercepat websitenya dan membuat tampilan ulang untuk menghasilkan user interface dan user experience terbaru."
date: 2019-05-07T19:30:21+07:00
simage: /img/portfolio/arumiflowershop.webp
image: /img/portfolio/sites/arumiflowershop.jpg
---

### [Arumi Flower Shop <i class="fal fa-external-link-square" aria-hidden="true"></i>] (https://arumiflowershop.com/)

Salah satu **Toko Bunga** yang berada di *Jakarta* ingin meningkatkan pendapatannya dengan cara mempercepat websitenya dan membuat tampilan ulang untuk menghasilkan user interface dan user experience terbaru.