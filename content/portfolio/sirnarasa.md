---
title: "Sirnarasa - Kajembaran Rohmaniyyah"
description: "Situs Pesantren Sirnarasa Cisirri, Ciomas, Panjalu, Ciamis - Jawa Barat."
type: "portfolio"
categories: ["Static HTML Site", "Blogging Site"]
date: 2017-09-08T20:02:13+07:00
simage: /img/portfolio/sirnarasa-s.webp
image: /img/portfolio/sites/sirnarasa.png
---

[Sirnarasa <i class="fal fa-external-link-square" aria-hidden="true"></i>] (https://www.sirnarasa.org) 

Pesantren Sirnarasa adalah tempat untuk belajar mendekatkan diri kepada Alloh SWT. Kajembaran Rohmaniyyah.