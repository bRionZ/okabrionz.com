---
title: "Indo Membrane"
description: "Dengan menggunakan teknologi terbaru dari AMP yang dikembangkan oleh Google demi mengejar target kecepatan maksimum. Kecepatan web Indo Membrane ini adalah sebuah mahakarya yang akan jadi referensi pembangunan web di okabrionz.com."
date: 2019-01-03T09:26:48+07:00
simage: /img/portfolio/indomembrane.webp
image: /img/portfolio/okabrionz.jpg
---
# Boosted by: AMP (Acelerated Mobile Page)

Dengan menggunakan teknologi terbaru dari AMP yang dikembangkan oleh Google demi mengejar target kecepatan maksimum. Kecepatan web Indo Membrane ini adalah sebuah mahakarya yang akan jadi referensi pembangunan web di okabrionz.com.

### [Tenda Membrane <i class="fal fa-external-link-square" aria-hidden="true"></i>] (https://www.indomembrane.com/) 