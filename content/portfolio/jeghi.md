---
title: "Jeghi.com - Konstruksi Baja Ringan"
description: "Solusi Konstruksi Baja Berat dan Baja Ringan Terpercaya dan Bergaransi"
type: "portfolio"
date: 2017-09-08T18:37:43+07:00
tags: ["",""]
categories: ["Static HTML Site", "Profil Perusahaan", "Landing Page"]
simage: /img/portfolio/jeghi-s.webp
image: /img/portfolio/sites/jeghi.jpg
---

[Jeghi.com <i class="fal fa-external-link-square" aria-hidden="true"></i>] (https://jeghi.com) 

"Terima kasih untuk Team Oka Brionz, Kami sekarang mempunyai website dan domain sendiri dan sangat profesional. Pengerjaan tepat waktu dengan revisi sepuasnya."