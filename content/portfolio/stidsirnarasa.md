---
title: "STID Sirnarasa - Sekolah Tinggi Ilmu Dakwah Sirnarasa"
description: "Situs Sekolah Tinggi Ilmu Dakwah (STID) Sirnarasa - Cisirri."
categories: ["Static HTML Site", "Landing Page", "Profil Sekolah"]
type: "portfolio"
date: 2017-09-08T20:02:18+07:00
simage: /img/portfolio/stid-sirnarasa-s.webp
image: /img/portfolio/sites/stidsirnarasa.jpg
---

### [STID Sirnarasa <i class="fal fa-external-link-square" aria-hidden="true"></i>] (https://www.stidsirnarasa.ac.id/) 