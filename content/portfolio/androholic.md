---
title: "Androholic"
description: "Androholic adalah blog yang membahas Tips & Tricks semua tentang android dengan sajian bahasa Indonesia."
type: "portfolio"
date: 2017-09-08T19:49:34+07:00
tags: ["",""]
categories: ["WordPress", "Blogging Site"]
simage: /img/portfolio/androholic-s.webp
image: /img/portfolio/sites/androholic.jpg
---

[AndroHolic <i class="fal fa-external-link-square" aria-hidden="true"></i>] (http://www.androholic.com)

Androholic dibuat untuk menyajikan informasi, tips & tricks tentang handphone yang menggunakan OS Android. Dibuat menggunakan CMS WordPress dan Theme dari mythemeshop.