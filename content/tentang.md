---
title: Tentang
description: Tentang Oka bRionZ, Mengenal lebih jauh siapa itu Oka bRionZ.
type: page
image: "/img/opengraph.jpg"
bahasa: id
date: 2017-12-01 00:00:00 +0700
lang: id

---
Saya adalah Internet Marketeer yang tinggal di Ciamis, Jawa Barat - Indonesia. Sangat terobsesi dengan Material Design buatan Angular atau biasa dikenal sebagai [Angular Material 4](https://material.angular.io) yang ingin membangun situs yang cepat dan maintenable (mudah di maintenance).

[Kecepatan loading website](https://www.okabrionz.com/jasa-memeprcepat-website) adalah hal yang pertama saya pertimbangkan ketika mengerjakan pembuatan situs milik klien. Berdasarkan pengalaman saya, tingkat stress seseorang meningkat ketika mengunjungi situs yang memerlukan banyak waktu ketika me-"render" sebuah web dengan waktu lebih dari 3 detik.

Saya juga ahli dalam membuat landing page supaya dapat di-index oleh mesin pencari seperti Google, Bing, Yahoo, & mesin pencari lainnya dengan lebih cepat dan optimal.

Saya menguasai bahasa pemrograman sebagai berikut, diantaranya:

* [HTML](https://id.wikipedia.org/wiki/HTML)
* [CSS](https://id.wikipedia.org/wiki/Cascading_Style_Sheets "Cascading Style Sheet") ([Sass](http://sass-lang.com/ "Syntatically Awesome Style Sheets") & [Less](http://lesscss.org/ "Less JS"))
* [Javascript](https://id.wikipedia.org/wiki/Javascript "Javascript") ([ES](https://en.wikipedia.org/wiki/ECMAScript "Ecma Script") & [Typescript](https://www.typescriptlang.org/ "Typescript Language"))
* [Angular](https://angular.io/ "Angular")
* [Hugo](https://gohugo.io/ "Static Site Generator")
* [Ember](https://www.emberjs.com/ "Ember JS")
* [Node](http://nodejs.org "Node JS")
* [React](https://reactjs.org/ "React")
* [AMP (Acelerated Mobile Page)](https://www.ampproject.org/ "AMP")

Peralatan yang saya gunakan, diantaranya:

* [Visual Studio Code](https://code.visualstudio.com/ "Visual Studio Code")
* [Sublime Text 3](https://www.sublimetext.com/3 "Sublime Text 3")
* [ConEmu](https://conemu.github.io/)
* [Git Bash](https://git-scm.com "Git Bash")
* [RIOT (Radical Image Optimization Tool)](http://luci.criosweb.ro/riot/ "Riot")
* [Google Chrome](https://www.google.com/chrome/browser/desktop/index.html "Chrome")
* [Browser Stack](https://www.browserstack.com/ "Browser Stack")