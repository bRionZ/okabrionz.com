---
title: 'Migrasi Situs Ke Bootstrap 4'
date: 2017-09-12T13:13:04+07:00
description: >-
    Migrasi dari Bootstrap 4 Alpha ke Bootstrap 4 Beta. Lumayan banyak perubahan menuju Bootstrap yang Stabil.
image: /img/posts/migrasi-bootstrap.jpg # img on post shortcode {{< img src="" alt="" height="766" width="512" layout="responsive" >}}
tags: ["Web Development", "CSS"]
bahasa: "id"
---

Akhirnya, Situs pribadi saya juga bisa migrasi ke Bootstrap 4 Beta. Lumayan lah agak repot juga waktu "debugging" grid layout nya sih, tapi setimpal sama kualitas yang tersaji.
Perubahan juga tersaji sangat menarik pada dokumentasi Bootstrap 4 Beta ini, sekarang mereka menggunakan sidebar dan fitur pencarian yang lebih keren. Dan juga, ada sedikit "bau" react didalamnya.

Yang ga kalah menarik buat di pantengin, Logo Bootstrap yang baru, dengan 3 tumpukan di bawah huruf "B" mungkin itu menandakan bahwa kita sudah berada di versi diatas 3.
Kombinasi dengan Creative-Tim, dan MDBootstrap yang semakin tampan dan sangat terlihat seperti situs masa kini. Pokok namah mantap we lah hahahay.