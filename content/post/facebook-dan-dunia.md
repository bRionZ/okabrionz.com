---
title: 'Facebook dan Dunia'
date: 2017-09-18T02:09:22+07:00
description: >-
    Mengenal lebih dalam tentang Jejaring Sosial Facebook. Pemirsa Facebook, Penggunaan Facebook, dan lainnya. 
image: /img/posts/fb-and-world.jpg # img on post shortcode {{< img src="" alt=""  width="678" height="380" layout="responsive" >}}
tags: ["Business", "World"]
bahasa: "id"
ampElement: comment
---

### Pemirsa Facebook

Dengan lebih dari 1,8 miliar pengguna aktif bulanan di seluruh dunia, Facebook menjangkau pemirsa sangat luas. Hal yang paling penting, pemirsa ini semakin sering menikmati video di platform Facebook baik Desktop maupun Selular, sebuah tren yang menciptakan peluang bercerita baru.

### Facebook dan Amerika Serikat

Orang Amerika Serikat menghabiskan satu dari lima menit waktu mereka di aplikasi selular Facebook dan Instagram. _Nielsen Mobile Netview, AS, Maret 2016_

### Facebook Video

Rata-rata 100 juta jam video dikonsumsi per hari di Facebook. _[Data Facebook](https://www.facebook.com/business/news/updated-features-for-video-ads)_.

### Pengguna di Seluruh Dunia

Sekitar 85% pengguna aktif Facebook dari luar Amerika Serikat dan Kanada. _[Kantor Berita Facebook](https://newsroom.fb.com/company-info/)_.

### Facebook menghubungkan 115 juta orang di Indonesia

{{< img src="/img/posts/indonesia-facebook.png" alt="Facebook di Indonesia" height="100" width="150" layout="responsive" >}}

Saat ini, 115 juta orang di Indonesia terhubung dengan teman, kerabat dan unit usaha baik kecil maupun besar1 di Facebook. Akses melalui ponsel merupakan pilihan utama, dimana 97% orang mengakses Facebook di gadget berlayar kecil ini. Mereka menggunakan Facebook tidak hanya untuk berbagi momen favorit tetapi juga untuk menyampaikan hal-hal yang penting bagi mereka, baik itu foto pantai Bali yang indah atau makan malam sederhana bersama keluarga.
Tetapi yang terutama, inilah hal - hal yang paling disukai komunitas pengguna Facebook.

### Indonesia senang berbagi

Baik itu mode atau makanan, kebugaran atau hari ulang tahun, komunitas yang berjumlah 115 juta orang ini, berbagi lebih banyak posting dari rata-rata pengguna global. Dan mengkomentari konten 60% lebih banyak daripada teman-teman lain di seluruh dunia.
Karena orang Indonesia terlibat secara aktif, muncul peluang untuk orang-orang menemukan produk dan layanan yang paling sesuai untuk mereka di Facebook.

### Indonesia senang komunitas

Komunitas Facebook Indonesia juga sangat tertarik untuk terhubung dan belajar lebih banyak tentang minat, hobi dan bahkan bisnis dan merek mereka.
Dua dari tiga orang Indonesia adalah bagian dari suatu grup, dengan grup terbesar di Facebook adalah komunitas jual beli, mobil dan hewan / hewan peliharaan.

### Indonesia senang merek

Di Indonesia, orang senang tetap berhubungan dengan bisnis dan merek selain keluarga dan teman mereka. Lebih dari 60% di antaranya terhubung dengan bisnis lokal, dan 42% orang di negara ini terhubung dengan setidaknya satu bisnis di negara lain.

Baik itu start up atau merek antar negara, orang Indonesia menyukai cerita dan produk dari bisnis. Perilaku ini tidak hanya terbatas pada kota-kota urban seperti Jakarta. Lebih dari 3 dari 4 pengguna Facebook di Indonesia berbasis di luar ibu kota dan tetap terhubung ke bisnis.

Orang Indonesia melakukan hal yang luar biasa di Facebook untuk saling mendukung, merayakan budaya Indonesia dan membuka peluang baru bagi diri mereka sendiri. Kami berkomitmen untuk membantu orang Indonesia berhubungan dengan orang, momen dan bisnis yang berarti bagi mereka.

{{< img src="/img/posts/facebook-indonesia.jpg" alt="Facebook di Indonesia" height="959" width="678" layout="responsive" >}}