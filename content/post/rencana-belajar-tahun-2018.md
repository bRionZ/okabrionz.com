+++
bahasa = "id"
date = "2017-12-26T15:53:53+00:00"
description = "Rencana pribadi untuk menjalani hidup di tahun 2018. Ini adalah catatan pribadi okabrionz.com untuk kedepan."
image = "/uploads/2017/12/26/rencana-2018.png"
tags = ["Programming"]
title = "Rencana Belajar Tahun 2018"
ampElement = "comment"

+++
### Tahun 2018 Akan Menjadi Tahun Yang Melelahkan

Ya! Melelahkan... Ini adalah alasan khusus buat ane pribadi ya. Karena belum dijalani, jadi pekerjaan tersebut terlihat sangat melelahkan.

### Januari - [Angular](https://angular.io "Angular")

Di awal tahun, ane pengin belajar bagaimana cara mengambil data pada Angular.

Data yang pengin ane ambil adalah  blog post yang dibuat pada file Markdown (.md)

Dapet info dari [grup telegram](https://t.me/AngularID "Angular Indonesia"), begini:

> Endy Muhardin, \[26.12.17 16:46\]
>
> Http request ke server, server keluarin json, trus render deh pake ngFor

Mudah-mudahan bisa diaplikasikan dari markdown to json terus di render pake ngFor di angular projeknya ya.

### Februari - Masih di [Angular](https://angular.io "Angular")

Kalo misalnya pembelajaran di bulan Januari sudah selesai, selanjutnya saya tinggal mempelajari cara pembuatan dashboard untuk membuat artikel, rencananya sih ane mau pakai [medium editor](https://github.com/yabwe/medium-editor "medium editor") yang kayaknya bakalan keren kalo jadi. Tapi, kalo misalnya ga memungkinkan, ya ane harus pake text editor yang lain. untuk sekarang belum nemu lagi yang bagus hehe.

### Maret - [Angular Routing](https://angular.io/tutorial/toh-pt5 "Angular Routing")

Angular routing itu keren banget. Karena adanya fungsi tersebut, kita bisa memproses perpindahan dari halaman ke halaman lain secara cepat, bahkan bisa disebut instant lho saking super cepatnya. Kenapa bisa cepet? karena yang kita request hanya file json saja (ga seluruh halaman).

Bulan selanjutnya pasti saya update lagi, postingan ini sebagai catatan pribadi untuk saya saja, tapi kalo Anda berminat belajar bareng-bareng, ya lebih bagus dong jadi ada temen deh.