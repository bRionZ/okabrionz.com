---
title: 'New Meta Description 2017 (DECription)'
date: 2017-12-31T23:24:51+07:00
description: 
    Harapan baru untuk SEO di tahun 2017 untuk 2018+. Kita diberikan hadiah dari Google yang berupa meta tag yang dapat tampil lebih banyak di Search Engine buatan Mbah Google akhirnya menambahkan karakter pada meta deskripsi di bulan Desember ini. Meringkas halaman dengan karakter lebih banyak pada SERP.
image: /img/posts/decription.png # img on post shortcode {{< img src="" alt="" height="100" width="100" layout="responsive" >}}
bahasa: "id"
tags: 
    - SEO
categories: 
    - Artikel
ampElement: comment
---

### Meta Deskripsi Update
Meta deskripsi adalah HTML atribut yang yang menyajikan ringkasan singkat halaman web. Deskripsi berada di antara satu kalimat ke paragraf pendek dan muncul di bawah tautan yang dapat diklik berwarna biru di halaman hasil mesin pencari (SERP). Namun, bergantung pada kueri pengguna, Google dapat menarik teks deskripsi meta dari area lain di laman Anda (untuk menjawab pertanyaan pencari pengguna dengan lebih baik). 

### Contoh Kode

```
<head>
    <meta name="description" content="Ini adalah contoh meta deskripsi yang akan tampil pada halaman hasil pencarian di mesin pencari.">
</head>
```

### Panjang Deskripsi Yang Optimal

Meta deskripsi dapat dibuat dengan berapapun banyaknnya karakter, namun Google secara umum memotong cuplikan hingga 300 karakter (batas ini meningkat pada bulan Desember 2017). Sebaiknya gunakan meta deskripsi yang panjang supaya dapat menjabarkan isi dari halaman tersebut, jadi sebaiknya karakter deskripsi berada di antara 50-300 karakter.ariasi tergantung pada situasi, dan tujuan utama Anda adalah memberikan nilai dan mengendalikan klik.

### Format Yang Optimal

Tag meta deskripsi, meski tidak terkait (ga ngaruh) pada peringkat mesin pencari, sangat penting untuk mendapatkan klik dari pengguna melalui SERP. Paragraf yang pendek ini adalah kesempatan webmaster untuk _"mengiklankan"_ konten kepada pencari untuk memutuskan apakah konten itu relevan dan berisi informasi yangmereka cari dari kueri penelusuran mereka.

Meta deskripsi halaman harus dibuat secara cedas (dengan cara yang alami, aktif, tidak spam) menggunakan kata kunci yang ditargetkan oleh halaman, tapi juga membuat deskripsi menarik yang akan diinginkan oleh pencari. Ini harus relevan secara langsung dengan halaman yang dideskripsikannya, dan unik dari deskripsi untuk halaman lain.

### Kesimpulan

Dengan adanya berita ini pasti ada juga pro dan kontra yang berdampak kepada pemilik web.

#### PRO
- Dapat menjabarkan lebih detail tentang isi halaman yang tampil pada halaman hasil pencarian Google.
- Meningkatkan kemampuan bermanuver untuk mendapatkan CTR (Click Through Rate).

#### Kontra
- Buat orang yang males nulis kaya saya, jelas ini nambah kerjaan dong pastinya. Ga panjang gimana, mau pendek kalah saing hehe.
- Harus menciptakan lebih banyak kata untuk mewakili suatu halaman.

Dan lain-lain yang belum kerasa.

Akhir kata, Semoga bermanfaat dan Wassalamualaikum..