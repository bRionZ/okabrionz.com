---
title: Cara Mempercepat Website
date: 2019-03-05T20:36:34+07:00
description: "Kecepatan website saat diakses oleh pengunjung merupakan faktor yang cukup penting. Pasalnya pengunjung cenderung memilih website yang memiliki akses yang paling mudah. Sebagus apapun konten atau produk Anda, rugi rasanya jika memiliki website yang lambat"
    
image: /img/posts/cara-mempercepat-website.png # img on post shortcode {{< img src="" alt="" height="100" width="100" layout="responsive" >}}
bahasa: id
tags: 
    - Teknologi
categories:
    - Artikel
draft: "false"
ampElement: "comment"
---

*Kecepatan website saat diakses oleh pengunjung merupakan faktor yang cukup penting. Pasalnya pengunjung cenderung memilih website yang memiliki akses yang paling mudah. Sebagus apapun konten atau produk Anda, rugi rasanya jika memiliki website yang lambat. Untuk itu bagi Anda yang mengelola toko online, blog maupun website perusahaan, faktor kecepatan website harus diperhatikan betul. Jangan sampai hal ini terlewat dari perencanaan pengembangan web kedepannya. Ada beberapa cara yang bisa Anda lakukan untuk mempercepat website Anda.*

### Pilih Provider Web Hosting Yang Tepat

Kualitas hosting menjadi salah satu faktor yang mempengaruhi cepat atau lambatnya sebuah website. Seorang digital marketing bernama Marcus Taylor pernah mengungkapkan bagaimana sebuah hosting memiliki pengaruh besar terhadap kecepatan website. Ceritanya ia mendapatkan dua klien yang memiliki website yang bisa dibilang mirip. Namun mereka memilih hosting yang berbeda, yang satu menggunakan dedicated server hosting dan satunya menggunakan shared-hosting. Alhasil, kedua website milik kliennya itu memiliki perbedaan kecepatan yang sangat signifikan.

### Optimasi Gambar-gambar Pada Situs Anda

Kurangnya optimasi pada gambar adalah penyebab utama memperlambat sebuah website. Optimalkan gambar Anda sebelum mengunggahnya ke website Anda. Cara optimal untuk mengoptimalkannya adalah dengan menggunakan Photoshop atau aplikasi editing lainnya. Anda juga harus menggunakan gambar progresif untuk file JPG karena rendering progresif gambar memberikan pengalaman pengguna.
Selalu gunakan theme original dan berkualitas
Jangan sampai Anda salah memilih theme yang akan digunakan untuk website Anda. Anda harus tahu betul kualitas dari theme tersebut. Selalu gunakan theme original yang dibeli dari developernya langsung. Hal tersebut bertujuan agar jika terjadi masalah dengan theme yang sudah dibeli, nantinya akan lebih mudah untuk diatasi. Theme yang ori akan mempengaruhi kecepatan website Anda karena biasanya akan selalu diperbaharui oleh sang developer.

### Gunakan CDN (Content Delevery Network)

Jika Anda memiliki website yang servernya berada di Indonesia pasti akan cepat diakses oleh pengunjung yang berasal dari Indonesia. Namun bagaimana jadinya jika pengunjungnya berasal dari Inggris? CDN inilah menjadi salah satu solusinya. CDN akan menyimpan konten Anda di server yang berada di berbagai negara di dunia. Jadi jika ada pengunjung yang berasal dari Inggris, maka server terdekatlah yang akan mengirim konten tersebut. Berikut adalah provider CDN terkenal di Dunia;

- Amazon CloudFront
- Akamai
- StackPath dari MaxCDN
- Cloudflare

Untuk memilih CDN Anda bisa menyesuaikan dengan kualitas atau dengan budget Anda.

### Optimasi database
Selama Anda memiliki sebuah website, sudah dipastikan Anda juga memiliki sebuah database. Teknologi inilah yang menyimpan data-data dari website Anda. Optimasi database berguna agar proses permintaan kepada database bisa lebih cepat yang akan berpengaruh pada kecepatan website Anda. Jika Anda menggunakan Wordpress, Anda hanya perlu menginstal [WP-Optimize](https://wordpress.org/plugins/wp-optimize/) yang bisa digunakan dengan sangat mudah walaupun Anda tidak mengetahui bahasa pemrograman sekalipun.

### Optimasi Cache Browser

Ada sumber menyebutkan bahwa Google merekomendasikan agar waktu cache minimal satu minggu hingga satu tahun.Aturlah kebijakan caching untuk semua respons server terhadap file statis seperti gambar, CSS, dan Javascript sehingga browser dapat menentukan apakah dapat menggunakan kembali file yang sebelumnya dimuat atau tidak. Ini biasanya dilakukan dengan menambahkan potongan kode ini pada file .htaccess untuk paket shared hosting.
Perhatikan ukuran setiap halaman website

Terkadang Anda tentu ingin memberikan informasi sedetail-detailnya kepada pengunjung. Salah satunya dengan cara menyediakan banyak konten di sebuah halaman web. Hal itu justru berpengaruh buruk tehadap kecepatan website. Halaman web akan terasa lambat saat diakses oleh pengunjung.

### Aktifkan Fitur **KeepAlive**

Didalam teknologi Apache terdapat banyak fitur hebat yang bernama KeepAlive. Fitur ini berguna untuk menjaga koneksi agar tetap terbuka untuk lebih dari satu permintaan HTTP. Pastikan perusahaan tempat Anda menyewa shared hosting mengaktifkan fitur yang satu ini. Anda juga bisa mengaktifkan KeepAlive jika Anda memiliki akses ke file httpd.conf. Selain itu Anda juga bisa menambahkan kode dibawah ini pada file .htaccess.
```
<ifModule mod_headers.c>
 Header set Connection keep-alive
 </ifModule>
 ```


### Mengurangi Waktu Respons Server

Kecepatan sebuah website juga dipengaruhi seberapa cepat waktu yang dibuthkan untuk mencari DNS Anda. DNS menyimpan IP maupun nama host yang saling terkait. Jika ada pengguna yang mengakses website melalui url, DNS akan memproses dengan merubah url tersebut menjadi alamat IP. 

**Itulah 10 cara mempercepat loading website. Sekali lagi, kecepatan website menjadi salah satu faktor kesuksesan bisnis Anda.**

### Atau, Gunakan Jasa Profesional

Khusus Anda yang tidak memiliki waktu yang cukup untuk riset ini-itu, atau sudah bingung walaupun banyak membaca referensi dan praktek namun kecepatan website masih loyo. Anda bisa menggunakan layanan [Jasa Mempercepat Website](jasa-mempercepat-website) yang kami sediakan. 