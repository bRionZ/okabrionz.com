---
title: Pentingnya Sebuah Nama Domain Pada Bisnis
date: 2019-02-19T02:16:48+07:00
description:
    Isi Deskripsinya boss
image: /img/posts/new-york.jpg # img on post shortcode {{< img src="" alt="" height="100" width="100" layout="responsive" >}}
bahasa: id
tags: 
    - Bisnis
    - Teknologi
categories:
    - Artikel
draft: "true"
ampElement:
---

Pada tingkat dasar, nama domain penting karena skema pengalamatan internet tidak terlalu efektif tanpanya.
Setiap komputer di Internet memiliki alamat Protokol Internet (IP): string unik empat angka yang dipisahkan oleh titik, seperti _165.166.0.2_ Karena mengingat alamat IP dari semua situs Web favorit Anda hampir mustahil, sekelompok ilmuwan komputer menciptakan sistem nama domain untuk menetapkan nama unik untuk setiap alamat IP numerik.