---
title: Cara Halaman Pertama Pada Hasil Pencarian Google
date: 2019-01-14T07:41:51+07:00
description:
    Cara mengoptimalkan halaman/situs untuk muncul di halaman pertama pada hasil pencarian Google. Trik SEO mudah dimengerti untuk pemula.
image: /img/posts/halaman-pertama-google.jpeg # img on post shortcode {{< img src="" alt="" height="100" width="100" layout="responsive" >}}
bahasa: id
tags: 
    - SEO
categories:
    - Artikel
ampElement: comment
---

Coba search _site:blogspot.com_ di Google. Ketika saya mencobanya beberapa waktu lalu, keluar hasil 321.000.000. Artinya ada 321.000.000 halaman web pada blogspot.com yang terindeks oleh Google. Tentu urusan ranking soal lain.

Tapi terindeks saja sudah sudah satu langkah positif. Beberapa diantara halaman-halaman tersebut memiliki ranking yang bagus. Meskipun pada blogspot.com, bukan pada website kita sendiri, memiliki halaman page dengan ranking bagus membawa banyak manfaat.

Lalu bagaimana caranya mendapatkan ranking yang tinggi dengan blogspot.com? Mari kita bedah karakteristik blog-blog blogspot.com yang sukses mendapatkan ranking yang baik untuk keyword-keyword populer. Apapun tujuan dari ranking yang tinggi tersebut, itu urusan belakang. Fokus kita sekarang bagaimana mendongkrak ranking blog blogspot.com di Google.

## K3? Konten, Kualitas, dan Konsistensi

Salah satu karakteristik utama dari blog-blog sukses ini adalah kualitas tulisan yang diposting secara konsisten. Kualitasnya konsisten, temanya konsisten, waktu postingnya konsisten. Masih berhubungan dengan kualitas konten adalah keaslian posting dan panjangnya yang umumnya lebih dari 400 kata.

Para pemilik blog tersebut mengarang tulisan yang bermanfaat dan meyakinkan untuk satu tujuan, memenuhi keinginan pembaca akan informasi. Para pembaca blog, terutama mereka yang datang melalui Google, mencari informasi yang berkualitas, dan itulah yang disediakan oleh blog-blog ini. Jika dibaca dengan teliti, tulisan pada blog-blog tersebut sama sekali jauh dari spamming, semua murni ditulis sebagai informasi yang bermanfaat bagi pembacanya.

Hal penting lainnya adalah jumlah halaman blog yang terindeks. Kebanyakan blog yang memiliki ranking yang bagus memiliki lebih dari 50 halaman yang terindeks, artinya pemiliknya sudah menulis lebih dari 50 posting untuk blog tersebut. Mereka yang memiliki ranking bagus untuk keyword yang kompetitif bahkan memiliki ratusan halaman yang terindeks.

Blog-blog ini umumnya tidak memuat terlalu banyak iklan. Anda mungkin sering memperhatikan banyak blog-blog yang dibuat semata-mata hanya untuk memuat AdSense, sehingga blog-blog tersebut penuh dijejali iklan tapi miskin konten, ini kesalahan besar yang sering dilakukan mereka yang ingin segera mendapatkan uang dengan [membuat blog](/jasa-pembuatan-blog/).

Blog yang memiliki ranking tinggi presentasi informasinya tidak diganggu oleh iklan. Sebagian bahkan tidak memiliki iklan. Kalaupun dipasangi iklan, penempatan iklannya diatur sedemikian rupa sehingga tidak mengganggu konten. Jumlahnya juga sangat terbatas. Disini dapat disimpuljan bahwa pare pemilik blog ini membuat dan mengisi blog mereka tidak semata-mata hanya mencari penghasilan dari iklan. Mereka menyediakan informasi yang bermanfaat bagi pembacanya.

Terakhir mereka tidak hanya menulis konten berkualitas sekali-sekali. Mereka secara konsisten meng-update dan menambah konten.

#### Apa yang bisa kita simpulkan disini adalah:

Pemilik blog yang memiliki ranking yang tinggi menulis konten berkualitas dalam jumlah besar. Blog yang memiliki ranking lumayan setidaknya memiliki 50 posting. Menulis konten blog bukanlah pekerjaan mudah. Selain sulit juga memakan waktu. Karena itu usahakan anda memamerkan penguasaan anda pada bidang spesifik tertentu. Jika kita menulis blog mengenai hobi tertentu, anda harus merupakan veteran pada hobi tersebut, bukan hobi yang baru anda mulai seminggu yang lalu. Ini akan membantu anda menghindari kehabisan ide untuk menulis. Selain itu, menulis mengenai sesuatu yang anda sukai akan membantu anda mengusir kebosanan. Kebanyakan blogger pengguna blogspot.com menulis bukan untuk mencari penghasilan, mereka menulis murni karena ingin berbagi pengetahuan dan pengalaman. Karena itu biasanya mereka selalu termotivasi untuk terus menulis.

Tulislah konten yang orisinil, bermanfaat, dan meyakinkan untuk penbaca. Ingat pepatah SEO ?content is king?. Anda perlu memperhatikan aspek ini jika anda ingin meningkatkan ranking anda. Google hanya mau meranking konten yang berkualitas, dan kriteria konten berkualitas adalah orisinil, informatif, dan bermanfaat. Konten berkualitas merupakan faktor penting dalam membangun link organik dalam jangka panjang, karena itu sangat penting untuk diperhatikan.

Konten tekstual tidak boleh terkesan spam, harus ditulis secara natural. Jangan ikut-ikutan membuat kesalahan seperti banyak blogger lain, menjejali posting mereka dengan keyword sebanyak-banyaknya.

Blog yang memiliki ranking tinggi membatasi iklan, baik dalam jumlah maupun penempatannya. Iklan harus dipisahkan dari konten. Pastikan bahwa konten tetap lebih dominan daripada iklan. Iklan yang terlalu mengganggu justru membuat blog anda terkesan seperti bukan sumber informasi yang berkualitas dan layak dipercaya oleh para pembacanya. Hal ini pada akhirnya mempengaruhi reputasi blog anda di mata search engine. Updatelah konten secara berkala.

Berikut ini saya list berbagai sumber backlink dari blog berbahasa Indonesia yang bisa anda manfaatkan untuk memaksimalkan posisi web Anda di pencarian Google.co.id :

### 1. Blogpost (Guest Post)

Backlink ini yang paling susah dicari, tapi menurut saya paling bagus kualitasnya. Mendapatkan backlink dari blogpost/artikel di web berbahasa lokal tentu sangat bagus untuk off-page web karena kita mendapat backlink dengan konten yang relevan. Agar backlink yang didapat lebih powerful, cari web yang sudah cukup berumur, mempunyai pembaca tetap, dan kalau bisa seniche dengan Money Site kita.

Biasanya saya membeli blogpost dari blogger-blogger diary (semoga Anda mengerti maksud saya), dengan alasan blog mereka belum banyak terkontaminasi dengan berbagai jenis monetize seperti blog-blog blogger matre :D Tidak masalah jika blog mereka masih numpang di blog gratisan seperti blogspot dan wordpress.

Yang perlu dicatat, biasanya tulisan blogger diary lebih mengalir, lebih natural dengan bahasa sehari-hari dan tentunya lebih unik. Dan yang jelas, blogpost di blog-blog seperti ini lebih berpeluang mendapatkan klik kunjungan daripada blogpost di blog khusus review (PTR). Orang bilang “Google follow people” dan menurut saya semakin sering suatu backlink mendapat klik (kunjungan) semakin besar pula kemungkinan backlink tersebut dianggap berkualitas oleh Google.

Untuk mendapatkan backlink jenis ini Anda bisa browsing komunitas-komunitas blogger lokal atau blogwalking di blog-blog lokal. Dari 1 blog yang Anda temukan Anda bisa dengan mudah menemukan blog-blog lainnya karena biasanya blogger-blogger diary ini sering bertukar komentar, tukar link, tukar award dan sebagainya.

### 2. Blogroll

Blogroll cukup powerful efeknya, tapi dengan resiko yang juga lebih besar. Kenapa ? Karena link di blogroll tidak berada dalam artikel. Jadi bisa saja disuatu halaman dia relevan dengan artikel yang ada dihalaman tersebut, tapi dihalaman lain (di blog yang sama) dia tidak relevan dengan isi artikel.

Blogroll juga termasuk massive link, apalagi jika ditempatkan di web yang sudah mempunyai ratusan atau ribuan halaman. Terkecuali jika Anda memasang blogroll dengan setting homepage saja (bukan semua halaman).

Untuk mendapatkan blogroll di blog lokal, caranya hampir sama dengan point 1 diatas (blogpost).

Dua hal yang perlu Anda ingat sebelum memutuskan membeli link di blogroll:
– Pastikan Money Site anda sudah kuat dan stabil untuk mendapat link yang cukup banyak dalam waktu bersamaan. Pedoman saya, biasanya jika blog sudah mendapat sitelink di Google. Jangan membuat blogroll link ketika Money Site masih berumur 1-2 bulan jika Anda tidak ingin dicaplok Penguin.
– Cari blog dengan niche yang relevan, agar massive link yang Anda dapat tidak terlalu spammy.

### 3. Komentar di Blog

Manual ataupun auto, pastikan prosentase komen lebih banyak di web dengan link komen dofollow. Untuk list nya anda bisa searching sendiri di Google.

### 4. Social Bookmark

Anda juga bisa searching sendiri list nya di Google. Yang kurang saya suka dari backlink ini adalah daftar websitenya yang cukup terbatas. Dan biasanya, web sosbok yang sudah populer merubah backlink mereka menjadi nofollow, memakai frame, menjejali iklan-iklan pop-up dan sebagainya. Dirty tricks and very annoying huh..

Jika Anda cukup telaten, Anda bisa browsing situs-situs social bookmark lokal yang masih baru, biasanya backlink di web mereka masih dofollow dan halaman tidak lemot karena kebanyakan banner.

### 5. Forum Post

Forum post disini maksudnya adalah backlink di situs-situs forum lokal, baik itu dengan membuat thread baru (Thread starter) maupun melalui komen di thread yang sudah eksis.

Beberapa forum lokal yang biasa saya pakai :

- Forum Tribun News
- Forum Abatasa
- Forum Kompas

Ketiga forum diatas mempunyai link dofollow, auto approve dan tentunya authority di root domainnya. Untuk forum lokal masih banyak list nya seperti Merdeka.com, Detik.com, Viva.co.id, indowebster dan sebagainya. Tapi sayangnya, kebanyakan di nofollow dan moderasi.

Untuk forum lokal lain yang dofollow dan auto approve bisa anda searching sendiri, saya yakin masih banyak yang lainnya.

### 6. Forum Signature / Profile

Sama seperti point no. 5, kali ini backlink di tempatkan di bagian Signature ataupun Profile (about me).

### 7. Web 2.0

Web 2.0 cukup banyak jumlahnya, antara lain :
- blogspot.com

- wordpress.com

- tumblr.com

- weebly.com

- livejournal.com

- Blogdetik.com

- LautanIndonesia.com, dsb.

Dengan membuat Web 2.0 sendiri sebagai blog dummy, Anda bisa membuat backlink dengan tipe blogpost maupun blogroll. Tentunya dengan konten utama bahasa Indonesia.

### 8. Membuat Blog Network Sendiri

Dengan membuat blog network sendiri kita lebih leluasa dalam memasang link, baik blogpost maupun blogroll. Kita juga bebas memilih niche yang digunakan, apakah disamakan dengan money site atau dibuat General. 

Apapun nama yang Anda gunakan, entah itu PBN (private blog network) dengan menggunakan expired domain, mini PBN, small PBN, atau Blog Dummy, konsep utamanya tetap sama, yaitu membuat network/jaringan dari blog-blog milik kita sendiri dan kita kontrol sendiri.

Semoga tips cara merangking web menjadi No. 1 di Google.co.id ini berguna bagi Anda, terutama yang sedang bergelut di bisnis lokal. Jangan lupa untuk mengimbangi dengan backlink luar (English contents) agar posisi web Anda di pencarian (baik .co.id maupun .com) semakin maksimal.

