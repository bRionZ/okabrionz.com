---
title: Hello AMP
date: 2017-09-11 12:42:55 +0700
description: Hello Dunia menuju Akhirat
tags:
- hello
- world
image: "/img/night.jpg"
bahasa: id
ampElement: comment
---
Akhirnya bisa bikin blog hugo pake AMP, Ahhaaii.
_"Lumayan lah seginih mah atuh"_. Mudah-mudahan dengan Blog AMP yang sekarang ini, situs saya semakin cepat.

Karena mimpi saya sebagai web developer adalah membuat situs tercepat di dunia. Bahkan saya berkeinginan mempunyai framework sendiri, namun apa daya sekarang ini hanya sekedar mimpi saja. Semoga kedepannya benar-benar bisa menggapai mimpi tersebut.
