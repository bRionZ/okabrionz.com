---
title: 'Cara Install .Net Framework 3.5 Offline'
date: 2017-09-16T02:59:01+07:00
description: Cara install .NET Framework 3.5 (includes .NET 2.0 and 3.0) secara offline atau tanpa koneksi internet dan tanpa download.
image: /img/posts/netframework-error.jpg # img on post shortcode {{< img src="" alt="" height="766" width="512" layout="responsive" >}}
tags: ["Tips & Tricks", "Windows", ".NET Framework"]
bahasa: "id"
ampElement: ["comment", "analytycs"]
---

### Berikut adalah Cara install .NET Framework 3.5 Offline dan Tanpa Download (Hemat kuota boss :D)

Tutorial ini ditulis karena waktu saya mau install software yang memerlukan .NET Framework 3.5 yang ternyata fiturnya belum terpasang di Laptop saya dan harus mendownload softwarenya dari windows.com.

Hmmm... males banget kalo saya harus pake kuota yang terbatas ini cuma buat nyobain software yang belum tentu saya perlu dan belum tentu juga saya bisa gunainnya. Untungnya ada solusi cerdas yang bisa dipake di saat kuota lagi genting ini.

Dengan tutorial ini, Anda pasti bisa menginstallnya tanpa perlu mendownload dan bahkan tanpa koneksi Internet sekalipun. Oke, langsung di gas aja lah.

Hal-hal yang Anda butuhkan untuk melakukan proses instalasi .NET Framework 3.5 dengan lancar dan langkah-langkahnya;

- Siapin Bootable Disk untuk instalasi Windows. Bisa berupa DVD atau FlashDisk (asal ada Windows Installernya aja).
- Jika Anda menggunakan DVD Installer Windows, masukin DVD ke Disk Drive
- Jika Anda menggunakan Bootable Disk (FlashDisk) Tingal di colokin aja itu Flashdisknya ke Laptop/PC.
- Jika Anda Cuma punya File Windows Mentah berupa .ISO File, maka tinggal di klik aja itu file installernya (ContohWindows-10.iso).
{{< img src="/img/posts/netframework-disk.jpg" alt="Cara Install .Net Framework 3.5 Offline" height="334" width="512" layout="responsive" >}}
- Setelah file bisa dibuka dan terbaca pada File Manager, Langkah selanjutnya Anda hanya perlu membuka CMD as Administrator. Caranya tinggal Klik-Kanan pada tombol Windows lalu klik Command Prompt (Admin) atau ```Windows+X lalu tekan C``` pada windows 10.
- Lalu, copy kode berikut dan pase pada Command Prompt yang sudah dibuka tadi. ```DISM.exe /Online /enable-feature /featurename:NetFX3 /Source:F:sources/sxs /LimitAccess```
- Jika Windows Installer/Bootable Disk/File .ISO terbuka pada folder lain selain Drive F, maka Anda hanya perlu mengganti kode pada ```/Source:F:sources/sxs``` dengan Drive yang ditampilkan. Jika Windows Installer nya terbuka pada Drive :D, maka Anda hanya perlu mengganti kode Source nya menjadi ```/Source:D:sources/sxs```.
{{< img src="/img/posts/netframework-cmd.jpg" alt="Cara Install .Net Framework 3.5 Offline" height="103" width="512" layout="responsive" >}}
- Tekan ```Enter``` dan tunggu sampai 100% atau sampai instalasinya selesai.

Semoga bermanfaat...