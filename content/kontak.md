---
title: "Kontak"
description: "Kontak Oka bRionZ, Hubungi Oka bRionZ."
type: "kontak"
thumbnail: 
bahasa: "id"
---

Silahkan untuk menghubungi saya dengan menggunakan formulir dibawah ini.

Anda juga dapat menghubungi saya via [Whatsapp](/whatsapp) atau [Telpon](tel:+6282319228666)
