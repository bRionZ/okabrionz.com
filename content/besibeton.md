---
title: 'Besi Beton'
date: 2019-10-29T12:02:14+07:00
type: "1page"
description: >-
    Cara menggunakan panel admin Besi Beton
image:  # img on post shortcode {{< img src="" alt="" height="766" width="512" layout="responsive" >}}
lang: "id"
---

### Cara Memasukan Snippet Tabel Harga

Karena tabel-tabel yang sebelumnya udah saya buat jadi shortcode, jadi agan tinggal memasukan shortcode tabel tersebut berdasarkan **ID** nya.

Contoh sebagai berikut:

`{{<besibeton-table>}}`

Untuk menambahkan tabel baru agan bisa bikin tabel harganya [disini](https://www.tablesgenerator.com/html_tables).
Disana Agan bisa bikin tabel harga baru atau bisa menambahkan tabel yang sudah ada dengan cara meng-copy tabel yang sudah ada, lalu paste di tablegenerator.com.

![Table Generator](/img/pages/besibeton1.jpg)

Setelah di Paste, lalu tabelnya di copy lagi dengan cara menekan tombol *Copy to clipboard*

![Table Generator](/img/pages/besibeton2.jpg)

Setelah di Copy, lalu Agan paste lagi code tersebut pada text editor yang ada di panel Admin besibeton. seperti berikut;

![Cara Membuat Table](/img/pages/besibeton3.jpg)

Jangan lupa untuk memindahkan mode pengeditan text ke RAW Editor terlebih dahulu sebelum menempelkan code table yang sudah di copy dari tablegenerator tadi.

Lalu di gambar ke-3 ada lingkaran yang diberi angka 2. Ganti code tersebut dengan code berikut;

`<table class="table">`

`<tr class="table-info">`

Sebelum melanjutkan menulis artikel/berita, silahkan mengganti lagi ke mode WYSIWYG Editor terlebih dahulu.

### Cara Mengubah Menu Navbar dan Footer Navigasi

Agan login terlebih dahulu, di panel terdapat menu, disitu ada keterangan seperti main menu, footer1, footer2, footer3. 

- **main** menu adalah menu yang terletak di bagian atas web.
- **footer1** menu 1 adalah bagian menu dibawah paling kiri.
- **footer2** menu bawah bagian tengah.
- **footer3** menu bawah bagian paling kanan.