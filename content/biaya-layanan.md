---
title: 'Biaya Layanan'
date: 2017-12-31T16:51:29+07:00
type: "1page"
description: 
    Estimasi Biaya untuk layanan jasa Pembuatan Website Profesional. Melayani pembuatan Company Profile, Toko Online, Landing Page, Web Presentasi, dan Situs Blog. Dan kami juga membuka jasa mempercepat loading website agar situs Anda tidak lemot sehingga mempengaruhi performa pada bisnis Anda.
image:   # img on post shortcode {{< img src="" alt="" height="766" width="512" layout="responsive" >}}
bahasa: "id"
---

Berikut ini adalah estimasi biaya pembuatan website standard yang kami buat dan tanpa modifikasi _(pesanan khusus)_.

Kami telah menjadikannya sebagai sebuah paket agar pembuatan website menjadi lebih terjangkau.
Apabila Anda menginginkan lebih dari ketentuan paket yang kami sediakan, maka kami akan mengenakan biaya tambahan sesuai tingkat kesulitan modifikasi yang Anda inginkan.

{{< rates >}}

Berikut adalah ketentuan-ketentuan yang kami buat,

1. Perencanaan awal tentang pembuatan web yang diinginkan.

2. Jika web Anda ingin dibuat secara _custom_ & unik, maka biaya tambahan akan dikenakan.
3. Proses pembayaran, Kami menyetujui adanya DP 50% dan sisanya dapat dilunasi setelah web Anda terima.

4. Biaya yang telah kami terima tidak akan dikembalikan sebagai uang jika kami telah memulai pekerjaan. Kami akan membuat web sesuai uang yang sudah masuk. Dengan kebijakan ini, Anda sebaiknya memantapkan untuk melangkah ke tahap selanjutnya.

5. Khusus untuk jasa pembuatan e-Commerce (#2) & Situs Blogging dengan WordPress, Anda harus memiliki sebuah hosting dan nama domain. Kami bersedia merekomendasikan hosting jika Anda belum mempunyai hosting & nama domain.