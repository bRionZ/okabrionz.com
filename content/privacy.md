---
title: Privacy
date: 2017-12-04 06:53:37 +0700
type: page
description: Ketentuan Privasi yang untuk para pengunjung okabrionz.com
image: "/uploads/2018/06/14/privacy.png"
lang: id
bahasa: ''

---
Terima kasih telah mempercayakan Oka bRionZ sebagai tujuan wisata digital pribadi Anda. Memegang informasi pribadi Anda adalah tanggung jawab yang serius, dan kami ingin Anda tahu bagaimana kami menanganinya.

### Versi pendeknya
Kami mengumpulkan informasi Anda hanya dengan persetujuan Anda; kami hanya mengumpulkan jumlah minimum informasi pribadi yang diperlukan untuk memenuhi tujuan interaksi Anda dengan kami; kami tidak menjualnya kepada pihak ketiga; dan kami hanya menggunakannya seperti yang dijelaskan oleh Pernyataan Privasi ini. Jika Anda mengunjungi kami dari UE: kami mematuhi kerangka Perisai Privasi. Tentu saja, versi singkatnya tidak memberi tahu Anda semuanya, jadi mohon baca terus untuk lebih jelasnya!

### Informasi apa yang dikumpulkan Oka bRionZ dan mengapa

#### Informasi dari web browser

Jika Anda **hanya melihat-lihat situs web**, kami mengumpulkan informasi dasar yang sama yang dikumpulkan oleh kebanyakan situs web. Kami menggunakan teknologi internet umum, seperti cookies dan log web server. Ini adalah barang yang kami kumpulkan dari semua orang, entah mereka punya akun atau tidak.


#### Mengapa kami mengumpulkan ini?

Kami mengumpulkan informasi ini untuk lebih memahami bagaimana pengunjung situs web kami menggunakan GitHub, dan untuk memantau dan melindungi keamanan situs web.

### Layanan-layanan dari pihak ke-3

Kami menggunakan layanan pihak ketiga untuk mendapatka data-data yang kami butuhkan seperti,

- [Google Analytics](https://www.google.com/analytics/)
- [HubSpot](https://www.hubspot.com/)
- [MailChimp](https://www.mailchimp.com/)
- [Tawk.to](https://tawk.to/)
